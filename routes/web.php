<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware(['web', 'auth'])->group(function () {
	Route::prefix('project')->group(function (){
	  Route::get('/', 'ProjectController@index')->name('project.index');
	  Route::get('/myprojects', 'ProjectController@mine')->name('project.mine');
	  Route::get('/create', 'ProjectController@create')->name('project.create');
	  Route::post('/create', 'ProjectController@store')->name('project.create');
	  Route::get('/edit/{id}', 'ProjectController@edit')->name('project.edit');
	  Route::post('/edit/{id}', 'ProjectController@update')->name('project.edit');
	  Route::get('/task/update/{id}', 'ProjectController@taskEdit')->name('project.task.edit');
	  Route::post('/task/update/{id}', 'ProjectController@taskUpdate')->name('project.task.edit');
	});
});

/*** Admin role end User creation routes **/
Route::get('users/roles', [
	'uses' => 'UserController@getRoles',
	'as' => 'roles',
	'middleware' => ['auth']
]);		
Route::get('users/list', [
	'uses' => 'UserController@index',
	'as' => 'user-list',
	'middleware' => ['auth']
	]);
Route::get('user/create', [
	'uses' => 'UserController@create',
	'as' => 'user-create',
	'middleware' => ['auth','is_admin']
	]);		
Route::post('user/create', [
	'uses' => 'UserController@store',
	'as' => 'user-create',
	'middleware' => ['auth','is_admin']
	]);	
Route::get('user/update/{id}', [
	'uses' => 'UserController@edit',
	'as' => 'user-edit',
	'middleware' => ['auth','is_admin']
	]);		
Route::post('user/update/{id}', [
	'uses' => 'UserController@update',
	'as' => 'user-edit',
	'middleware' => ['auth','is_admin']
	]);		
Route::delete('user/delete/{id}','UserController@destroy')->name('user-delete');	