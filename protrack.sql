-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 16, 2021 at 03:10 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `protrack`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2021_02_15_141649_create_projects_table', 1),
(4, '2021_02_15_141958_create_tasks_table', 1),
(5, '2021_02_15_142027_create_roles_table', 1),
(6, '2021_02_15_142104_create_task_status_table', 1),
(7, '2021_02_15_143115_create_task_assign_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '1',
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `name`, `description`, `status`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'fsdfs', 'dsfdsfd', 1, 1, '2021-02-16 00:56:55', '2021-02-16 00:56:55'),
(2, 'sekkak', 'ksdjfks des', 1, 1, '2021-02-16 01:08:36', '2021-02-16 01:08:36'),
(3, 'sampel new proj updates', 'test with suer updates', 1, 3, '2021-02-16 01:19:19', '2021-02-16 03:57:10'),
(4, 'Codeigneter and testing', 'testing', 1, 4, '2021-02-16 04:09:14', '2021-02-16 04:09:14'),
(5, 'Manage ecommerce project of ITECH', 'TESTING', 1, 4, '2021-02-16 07:13:23', '2021-02-16 07:13:23');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', '2021-02-16 00:08:18', '2021-02-16 00:08:18'),
(2, 'Author', '2021-02-16 00:08:18', '2021-02-16 00:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `tasks`
--

CREATE TABLE `tasks` (
  `id` int(10) UNSIGNED NOT NULL,
  `proj_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `task_status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `task_assign_users`
--

CREATE TABLE `task_assign_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `proj_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_assign_users`
--

INSERT INTO `task_assign_users` (`id`, `proj_id`, `task_id`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(24, 3, 1, 2, 0, '2021-02-16 04:15:53', '2021-02-16 04:15:53'),
(25, 3, 1, 4, 0, '2021-02-16 04:15:53', '2021-02-16 04:15:53'),
(26, 3, 2, 2, 0, '2021-02-16 04:15:53', '2021-02-16 04:15:53'),
(27, 3, 2, 3, 0, '2021-02-16 04:15:53', '2021-02-16 04:15:53'),
(28, 3, 3, 1, 0, '2021-02-16 04:15:53', '2021-02-16 04:15:53'),
(29, 3, 3, 3, 0, '2021-02-16 04:15:54', '2021-02-16 04:15:54'),
(30, 4, 2, 3, 0, '2021-02-16 06:33:27', '2021-02-16 06:33:27'),
(31, 4, 2, 4, 1, '2021-02-16 06:33:27', '2021-02-16 06:56:34'),
(32, 4, 3, 2, 0, '2021-02-16 06:33:27', '2021-02-16 06:33:27'),
(33, 4, 3, 3, 0, '2021-02-16 06:33:27', '2021-02-16 06:33:27'),
(34, 4, 4, 4, 0, '2021-02-16 06:33:28', '2021-02-16 07:09:00'),
(35, 5, 2, 2, 0, '2021-02-16 07:13:23', '2021-02-16 07:13:23'),
(36, 5, 2, 3, 0, '2021-02-16 07:13:23', '2021-02-16 07:13:23'),
(37, 5, 3, 3, 0, '2021-02-16 07:13:23', '2021-02-16 07:13:23'),
(38, 5, 4, 5, 0, '2021-02-16 07:13:23', '2021-02-16 07:13:23');

-- --------------------------------------------------------

--
-- Table structure for table `task_status`
--

CREATE TABLE `task_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `config` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `task_status`
--

INSERT INTO `task_status` (`id`, `name`, `config`, `created_at`, `updated_at`) VALUES
(1, 'Not Started', 0, '2021-02-16 00:08:18', '2021-02-16 00:08:18'),
(2, 'Authoring', 1, '2021-02-16 00:08:18', '2021-02-16 00:08:18'),
(3, 'Reviewing', 1, '2021-02-16 00:08:18', '2021-02-16 00:08:18'),
(4, 'Approving', 1, '2021-02-16 00:08:18', '2021-02-16 00:08:18'),
(5, 'Done', 0, '2021-02-16 00:08:18', '2021-02-16 00:08:18');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@example.com', NULL, '$2y$10$CQ4YSkNlD/2YNHSANjAwkOIUv0iVfOCEh35i36908JRkMkyYazm96', 1, 'yMIm25HsZtIYr6hBeatPiXF1n4Xr4eNg52Nb2zmxww7FAkbCnI0idw2CChEN', '2021-02-16 00:08:18', '2021-02-16 00:08:18'),
(2, 'Vinesh Suresh', 'vinesh@example.com', NULL, '$2y$10$gE7zeif1V4tg40YxYaJ8Fenh8l.txw0Pb0yg0JC9pm1XJ1oYhIXKG', 2, 'YymFSK9o6ehmer5iRy9ue272svrMo5dfEECyNRxPTXAn5X5RMxwte2JRcBJ4', '2021-02-16 00:15:06', '2021-02-16 07:54:23'),
(3, 'Kumar', 'kumar@example.com', NULL, '$2y$10$mTFTwOBu4grKLDFFEtmSOu.U3IUczlXlNpGsbcYeyZrT2T.Cajm6.', 2, 'hy359AaFhVnv6MsOhbTUCyM9xHhIyAAPjyRlJASP73V9rI4Dp8GTwqnXwyXw', '2021-02-16 00:28:56', '2021-02-16 00:28:56'),
(4, 'Author', 'author@example.com', NULL, '$2y$10$rb/NkGr.tLYw3rW.50PqyOzqGqeVGSiT2jLDCuI3PoxQNFkd3hfzu', 2, 'hJetIk0hz0JBS2iHo0OOEeGhZ9UPTgIaJzo500M92UDzFfoe344orjDryYKM', '2021-02-16 04:08:30', '2021-02-16 04:08:30'),
(5, 'Suresh', 'suresh@example.com', NULL, '$2y$10$WaeFJ1nw2Kctxbg3YyaeUOzlqo9Np/6rq4uTtt9vZwomxmOlEy4iS', 2, NULL, '2021-02-16 07:12:10', '2021-02-16 07:12:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_assign_users`
--
ALTER TABLE `task_assign_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `task_status`
--
ALTER TABLE `task_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `task_assign_users`
--
ALTER TABLE `task_assign_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `task_status`
--
ALTER TABLE `task_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
