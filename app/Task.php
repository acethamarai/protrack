<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
   protected $table = 'tasks';
    
    protected $fillable = ['proj_id', 'description', 'task_status_id'];

    public function projects()
    {
      return $this->belongsTo('App\Project', 'proj_id','id');
    }
    
    public function status()
    {
    	return $this->hasOne('App\TaskStatus', 'task_status_id','id');
    }

}
