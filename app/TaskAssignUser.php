<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskAssignUser extends Model
{
    
    protected $table = 'task_assign_users';  

    protected $fillable = [
        'proj_id','task_id','user_id','status'
    ];

    
}
