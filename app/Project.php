<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    
    protected $table = 'projects';  

    protected $fillable = [
        'name','description','status','user_id'
    ];

    public function tasks()
    {
    	return $this->hasMany('App\Task','proj_id');
    }

    public function author()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function statuscode()
    {
        return $this->belongsTo('App\TaskStatus','status','id');
    }

    public function taskassign()
    {
    	return $this->hasMany('App\TaskAssignUser','proj_id');
    }
}
