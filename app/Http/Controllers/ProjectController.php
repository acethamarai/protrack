<?php

namespace App\Http\Controllers;

use App\User;
use App\Project;
use App\Task;
use App\TaskStatus;
use App\TaskAssignUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use Auth;
use Session;

class ProjectController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        if(Auth::user()->role == 1){
            $projects = Project::orderBy('id','ASC')->get();
        }else{
            //Author owner projects
            $usr_projects = Project::where('user_id',Auth::user()->id)->orderBy('id','ASC')->get();
            if(count($usr_projects) > 0){
                foreach($usr_projects as $val){ $proja[] = $val->id; }
            }else{
                $proja = array();
            }
           
            //Assigned user projects
            $users =DB::table('task_assign_users')->where('user_id',Auth::user()->id)->select('proj_id')->groupBy('proj_id')->get();
            if(count($users) > 0){
                foreach($users as $val){ $projb[] = $val->proj_id; }
            }else{
                $projb = array();
            }
            //combine and eliminate duplicate
            $new = array_merge($proja,$projb);
            $projects = array_unique($new);
            $projects = Project::whereIn('id',$projects)->orderBy('id','ASC')->get();
        }
        return view("projects.index",compact("projects"));
    }

    public function mine()
    {
        $projects = Project::where('status','!=',4)->orderBy('id','ASC')->get();
        return view("projects.myindex",compact("projects"));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$status = TaskStatus::where('config',1)->get();
		$users = User::get();
        return view('projects.create',compact("status","users"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validations
        $data = $this->validate($request,[
            'name' => 'required|unique:projects,name'
        ]);
       
        $status = TaskStatus::where('config',1)->get();
        foreach($status as $val){
            if(isset($request['task_status_'.$val->id])){ 
                $task_valid='true';
            }else{
                $task_valid='false';
                return redirect()->back()->with('warning', 'Some tasks not assigned users');
            }
        }

        //Value insert into DB
        $input = $request->all();
        $data = Project::create($input);
        $proj_id = $data->id;

        foreach($status as $val){
            foreach($request['task_status_'.$val->id] as $v){
            $formData = new TaskAssignUser();
            $formData->proj_id = $proj_id;
            $formData->task_id = $val->id;
            $formData->user_id = $v;
            $formData->status = 0;
            $formData->save();
            }
        }

        Session::flash('status','Added Successfully !');
        return redirect()->route('project.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::find($id);
        if(Auth::user()->role != 1 && $project->user_id != Auth::user()->id){
            return abort(404);
        }
        $status = TaskStatus::where('config',1)->get();
		$users = User::get();
        return view('projects.edit',compact("project","status","users"));
    }

    private function getAssignUsers($taskid,$projid){
        $users = TaskAssignUser::where('proj_id',$projid)->where('task_id',$taskid)->get();
        return $users;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request,$id)
    {

        //Validations
        $data = $this->validate($request,[
            'name' => 'required|unique:projects,name,'.$id,
        ]);
       
        $status = TaskStatus::where('config',1)->get();
        foreach($status as $val){
            if(isset($request['task_status_'.$val->id])){ 
                $task_valid='true';
            }else{
                $task_valid='false';
                return redirect()->back()->with('warning', 'Some tasks not assigned users');
            }
        }

        //Value insert into DB
        $prodata = Project::findorfail($id);
        $input = $request->all();
        $prodata->update($input);

        TaskAssignUser::where('proj_id',$id)->delete();
        foreach($status as $val){
            foreach($request['task_status_'.$val->id] as $v){
            $formData = new TaskAssignUser();
            $formData->proj_id = $id;
            $formData->task_id = $val->id;
            $formData->user_id = $v;
            $formData->status = 0;
            $formData->save();
            }
        }

        Session::flash('status','Updated Successfully !');
        return redirect()->back();     
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\slider  $slider
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        $cate = Project::find($id);
        $value = $cate->delete();

        if($value)
        {
            session()->flash("delete","Project has been deleted");
            return redirect()->back();  
        }

    }

    public function taskEdit($id)
    {
        if(Auth::user()->role == 1){
            return abort(404);
        }
        $project = Project::find($id);
        $status = TaskStatus::where('config',1)->get();
		//$users = User::get();
        return view('projects.taskedit',compact("project","status","users"));
    }

    public function taskUpdate(Request $request,$id)
    {

        //Validations
        $status = TaskStatus::where('config',1)->get();
        $approve_order = false;
		$approve_current = false;
		$start = 1;
        foreach($status as $key => $val){
            $all_status = TaskAssignUser::where('proj_id',$id)->where('task_id',$val->id)->count();
            $yes_status = TaskAssignUser::where('proj_id',$id)->where('task_id',$val->id)->where('status',1)->count();
            if($all_status == $yes_status){
                $approve_order = true;
				//continue;
				$start++;
            }else{
				$approve_order = false;
				$start--;
			}
			
			$formData = TaskAssignUser::where('proj_id',$id)->where('task_id',$val->id)->where('user_id',Auth::user()->id)->first();
			if(($approve_order == true || $start > 1) && $formData != NULL){
				$formData->status = $request['task_status_'.$val->id];
				$formData->save();
				$approve_order = true;
				$approve_current = true;
			}else{
				$approve_order = false;
				$start--;
			}
		} 
        
        if($approve_current == true){
            Session::flash('status','Updated Successfully !');
        }else{
            Session::flash('warning','Some previous task status are pending !');
        }
        return redirect()->back();     
    }
}
