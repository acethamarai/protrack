<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\Role;
use DB;
use Hash;
use URL;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
	
	/**
     * Display a listing of roles in app.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRoles()
    {
        $users = Role::all();

        return view('roles.index', compact('users'));
    }
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$validatedData = $request->validate([
			'name' => 'required',
			'email' => 'required|unique:users',
			'password' => 'required',
			'role' => 'required',
		]);
        
        $input = $request->all();
        $input['password'] = Hash::make($request->password);  
        $data = User::create($input);
        return redirect()->route('user-list');
    }
	
	public function edit($id)
    {
        $roles = Role::get();
        $user = User::find($id);
       
        return view('users.edit', compact('user','roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
		$validatedData = $request->validate([
			'name' => 'required',
			'role' => 'required',
		]);
        
        $prodata = User::findorfail($id);
        $input = $request->all();
        $input['password'] = Hash::make($request->password);  
        $prodata->update($input);
        return redirect()->route('user-list');
    }

    public function destroy($id)
    {
        $cate = User::find($id);
        $value = $cate->delete();

		Project::where('user_id',$id)->delete();
		TaskAssignUser::where('user_id',$id)->delete();
		
        if($value)
        {
            session()->flash("delete","User has been deleted");
            return redirect()->back();  
        }

    }

}
