<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TaskStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
        \DB::table('task_status')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Not Started',
                'config' => 0,
                'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Authoring',
                'config' => 1,
                'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
            ),
			2 => 
            array (
                'id' => 3,
                'name' => 'Reviewing',
                'config' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Approving',
                'config' => 1,
                'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Done',
                'config' => 0,
                'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s')
            )
		));	
      
    }
}
