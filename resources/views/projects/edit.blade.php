@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update Project</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
					@if (session('warning'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('warning') }}
                        </div>
                    @endif
					@if ($errors->any())
					<div class="alert alert-danger alert-dismissible fade in">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
				  <form method="post" action="{{route('project.edit',['id' => $project->id])}}">
				  {{ csrf_field() }}
					<div class="form-group">
					  <label for="email">Project Name:</label>
					  <input type="text" class="form-control" name="name" value="{{$project->name}}" required >
					</div>
					<div class="form-group">
					  <label for="pwd">Description:</label>
					  <textarea class="form-control" name="description">{{$project->description}}</textarea>
					</div>
					<div class="form-group">
					  <label>Tasks</label>
					  <table class="table">
					  
					  @foreach($status as $val)
					  <tr id="row_{{$val->id}}"><td>{{$val->name}}</td>
					  <td>
					  <select class="selectpicker" name="task_status_{{$val->id}}[]" multiple data-live-search="true">
						@foreach($users as $usr)
						@php
						$echk = App\TaskAssignUser::where('proj_id',$project->id)->where('task_id',$val->id)->where('user_id',$usr->id)->first();
						@endphp
						<option value="{{$usr->id}}" @if($echk != NULL){{'selected'}}@endif>{{$usr->name}}</option>
						@endforeach
					</select>
					</td>
					  @endforeach
					  </table>
					  

					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

@endsection
