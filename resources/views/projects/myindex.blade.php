@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if(count($projects) > 0)
					<table class="table table-bordered">	
					<tr><th>#</th><th>Project Name</th><th>Description</th><th>Author</th><th>Status</th></tr>
					@php $sn=1; @endphp
					@foreach($projects as $val)
					<tr><td>{{$sn}}</td><td>{{$val->name}}</td><td>{{$val->description}}</td><td>{{$val->author->name}}</td><td>{{$val->statuscode->name}}</td></tr>
					@php $sn++; @endphp
					@endforeach
					</table>
					@endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
