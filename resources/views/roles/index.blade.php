@extends('layouts.app')
@section('content')
<style>

</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List of Roles</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
				
				  <table class="table table-bordered " id="users-table">
				  <thead>
					<tr>
					   <th>Sl. No</th>
					   <th>Name</th>
					   <th>Actions</th>
					   
				   </tr>
				  </thead>
				   <tbody>
                    @if (count($users) > 0)
						<?php $sn=1; ?>
                        @foreach ($users as $user)
                            <tr data-entry-id="{{ $user->id }}">
                                <td>{{$sn}}</td>
                                <td>{{ $user->title }}</td>
                                <td>
                                    <a href="" class="btn btn-xs btn-info">Edit</a>
                                   
                                </td>
                            </tr>
						<?php $sn++; ?>	
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">roles not found.</td>
                        </tr>
                    @endif
                </tbody>
				</table>
				
				 </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>		
@endsection
