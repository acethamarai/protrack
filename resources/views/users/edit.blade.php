@extends('layouts.app')
@section('content')
<style>

</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Update User</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                @if ($errors->any())
                <div class="alert alert-danger alert-warning" role="alert">
                  <ul>
                    @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                    @endforeach
                  </ul>
                </div>
                @endif
                @if (session('warning'))
                  <div class="alert alert-warning" role="alert">
                    {!! session('warning') !!}
                  </div>
                @endif	
				  <form role="form" class="form-horizontal form-label-left" method="post" action="{{ route('user-edit',['id' => $user->id]) }}" enctype="multipart/form-data" >
			  	{{ csrf_field() }}
            
              <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right" for="name">Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6">
                    <input type="text" id="name" name="name" required="required" class="form-control" value="{{$user->name}}" autocomplete="off">
                  </div>
                </div>
    
              <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right" for="email">Email Address <span class="required">*</span>
                  </label>
                  <div class="col-md-6">
                    <input type="email" id="email" name="email" required="required" class="form-control" value="{{$user->email}}" readonly autocomplete="off" >
                  </div>
                </div>
      
              <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right" for="password">Password <span class="required">*</span>
                  </label>
                  <div class="col-md-6">
                    <input type="password" name="password" id="password" minlength="6" required="required" class="form-control" autocomplete="new-password" >
                  </div>
                </div>
            
              <div class="form-group row">
                  <label class="col-md-4 col-form-label text-md-right" for="roles">Role <span class="required">*</span>
                  </label>
                  <div class="col-md-6">
                    <select name="role" id="role" class="form-control" required>
                  @foreach($roles as $key => $val)
                  <option value="{{$val->id}}" @if($user->role == $val->id){{'selected'}}@endif>{{$val->name}}</option>
                  @endforeach
                  </select>
        
                  </div>
                </div>
          <div class="form-group row">
					<label class="col-md-4 col-form-label text-md-right" for="password">
                        </label>
						<div class="col-md-6">
							<button class="btn btn-primary" type="submit" tabindex="10"><i class="ace-icon fa fa-check bigger-110"></i>Submit</button>
							&nbsp; &nbsp; &nbsp;
							<button class="btn" type="reset" tabindex="11"><i class="ace-icon fa fa-undo bigger-110"></i>Reset</button>
						</div>
					</div>
				 </form>
				  </div>
            </div>
        </div>
    </div>
</div>

<script>		

</script> 		
@endsection
