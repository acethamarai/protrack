@extends('layouts.app')
@section('content')
<style>

</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">List of Users</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
				
				  <table class="table table-bordered " id="users-table">
				  <thead>
					<tr>
					   <th>Sl. No</th>
					   <th>Name</th>
					   <th>Email</th>
					   <th>Role</th>
                       @if(Auth::user()->role == 1)
					   <th>Action</th>
                       @endif
				   </tr>
				  </thead>
				   <tbody>
                    @if (count($users) > 0)
						<?php $sn=1; ?>
                        @foreach ($users as $user)
                            <tr data-entry-id="{{ $user->id }}">
                                <td>{{$sn}}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->rolename->name }}</td>
                                @if(Auth::user()->role == 1)
                                <td>
                                <a href="{{route('user-edit',['id' => $user->id])}}">Edit</a>
                                @if(Auth::user()->id != $user->id)
                                <form  method="post" action="{{ route('user-delete',$user->id) }}
                                "data-parsley-validate class="form-horizontal form-label-left">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button style="border:none;outline:none" onclick="return confirm('Are you sure you want to delete?')" type="submit" class="btn-trans">Delete</button>
                                </form>
                                @endif
                                </td>
                                @endif
                            </tr>
						<?php $sn++; ?>	
                        @endforeach
                    @else
                        <tr>
                            <td colspan="5">users not found.</td>
                        </tr>
                    @endif
                </tbody>
				</table>
				
				 </div>
            </div>
        </div>
    </div>
</div>
<script>

</script>		
@endsection
