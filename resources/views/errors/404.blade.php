@extends('layouts.app')
@section('content')

	<div class='col-lg-12 col-lg-offset-6'>
		<h1 style="color:#000"><center>404<br>
		PAGE NOT FOUND</center></h1>
		<div class="align-center"></div>
		<div class="align-center" style="color:#000;text-align:center"><a href="{{route('home')}}" class="btn btn-default">Go Dashboard</a></div>
	</div>
@endsection