@extends('layouts.app')
@section('content')
	<div class='col-lg-4 col-lg-offset-4'>
		<h1 class="missh1"><center>401<br>
		ACCESS DENIED</center></h1>
		<div><img class="missdiv" src="{{ asset('public/images/missing-piece.png') }}" alt="Missing" class="401-logo" /></div>
	</div>
@endsection